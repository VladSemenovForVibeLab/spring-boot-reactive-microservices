# Руководство по использованию Spring Boot Reactive Microservices для микросервиса product-microservice

Данное руководство описывает, как использовать Spring Boot Reactive Microservices для разработки микросервиса
product-microservice с использованием Spring Data Reactive MongoDB и Lombok.


## Установка

1. Установите Java Development Kit (JDK) версии не ниже 11.
2. Скачайте и установите Maven, если его ещё нет на вашем компьютере: https://maven.apache.org/install.html
3. Скачайте и установите MongoDB, если его ещё нет на вашем компьютере: https://www.mongodb.com/try/download/community

## Использование

1. Склонируйте репозиторий с микросервисом product-microservice:

   ```
   git clone git@gitlab.com:VladSemenovForVibeLab/spring-boot-reactive-microservices.git
   ```

2. Перейдите в каталог проекта:

   ```
   cd product-microservice
   ```

3. Соберите проект с помощью Maven:

   ```
   mvn clean package
   ```

4. Запустите MongoDB:

   ```
   mongod
   ```

5. Запустите микросервис:

   ```
   java -jar target/product-microservice-1.0.0.jar
   ```

6. Микросервис будет доступен по адресу `http://localhost:8091`.

## Использование API

После запуска микросервиса, вы сможете взаимодействовать с API микросервиса с помощью HTTP-методов.

Примеры запросов:

1. Получение списка всех продуктов:

   ```
   GET http://localhost:8091/product/all
   ```

2. Создание нового продукта:

   ```
   POST http://localhost:8091/product
   Content-Type: application/json

   {
    "description":"{{$randomLoremText}}",
    "price":"23012
   }

   ```

5. Обновление существующего продукта:

   ```

    PUT http://localhost:8091/product/{id}
    Content-Type: application/json

    {
    "description":"{{$randomLoremText}}",
    "price":"12335"
    }

   ```

6. Удаление продукта:

   ```

    DELETE http://localhost:8091/product/{id}

   ```

## Заключение

Вы успешно настроили и запустили микросервис product-microservice с использованием Spring Boot Reactive Microservices, Spring Data Reactive MongoDB и Lombok. Теперь вы можете разрабатывать и использовать данный микросервис для вашего проекта.